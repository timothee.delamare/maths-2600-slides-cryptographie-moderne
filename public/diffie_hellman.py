import random


def diffie_hellman_keygen(prime_number: int, generator: int) -> dict:
    # Implementez la generation de clef de diffie hellman
    # 1. Tire aléatoirement un nombre secret a entre 2 et prime_number - 1 inclus avec random.randint(min, max)
    private_key = random.randint(2, prime_number - 1)

    # 2. Calcule la clef publique A = generator^a mod prime_number
    public_key = pow_mod(generator, private_key, prime_number)

    # 3. Renvoit le dictionnaitre {"public_key": A, "private_key": a}
    return {"public_key": public_key, "private_key": private_key}


def diffie_hellman_compute_shared_secret_key(public: int, private: int, prime_number: int) -> int:
    # Implementez le calcul de la clef secrete partagée à partir de la clef publique de l'autre participant et de ma clef privée
    return pow_mod(public, private, prime_number)


def orchestrate_diffie_hellman_protocol():
    p = 32317006071311007300153513477825163362488057133489075174588434139269806834136210002792056362640164685458556357935330816928829023080573472625273554742461245741026202527916572972862706300325263428213145766931414223654220941111348629991657478268034230553086349050635557712219187890332729569696129743856241741236237225197346402691855797767976823014625397933058015226858730761197532436467475855460715043896844940366130497697812854295958659597567051283852132784468522925504568272879113720098931873959143374175837826000278034973198552060607533234122603254684088120031105907484281003994966956119696956248629032338072839127039
    g = 2

    # Orchestrez diffie-hellman entre 2 participants
    # Verifiez que la clef secrete partagée calculée par les 2 est identique
    alice_key = diffie_hellman_keygen(p, g)
    print("Alice", alice_key)

    bob_key = diffie_hellman_keygen(p, g)
    print("Bob", bob_key)

    bob_side_shared_key = diffie_hellman_compute_shared_secret_key(alice_key["public_key"], bob_key["private_key"], p)
    alice_side_shared_key = diffie_hellman_compute_shared_secret_key(bob_key["public_key"], alice_key["private_key"], p)

    print(bob_side_shared_key)
    print(alice_side_shared_key)

    assert bob_side_shared_key == alice_side_shared_key


def pow_mod(B, E, M):
    if E == 0:
        return 1
    elif E == 1:
        return B % M
    else:
        root = pow_mod(B, E // 2, M)
        if E % 2 == 0:
            return (root * root) % M
        else:
            return (root * root * B) % M


orchestrate_diffie_hellman_protocol()
